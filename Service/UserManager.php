<?php

namespace App\Service;

use App\Entity\Administrator;
use App\Entity\BaseUser;
use App\Entity\Customer;
use App\Entity\Seller;

class UserManager implements UserManagerInterface
{
    /**
     * @param BaseUser $user
     *
     * @return string
     *
     * @throws \Exception
     */
    public function getUserInfo(BaseUser $user): string
    {
        $userType = get_class($user);
        switch ($userType) {
            case Customer::class:
                return $this->getCustomerInfo($user);
            case Seller::class:
                return $this->getSellerInfo($user);
            case Administrator::class:
                return $this->getAdministratorInfo($user);
            default:
                throw new \Exception(\sprintf('%s class does\'t supported!', $userType));
        }
    }

    /**
     * @param Customer $user
     *
     * @return string
     */
    private function getCustomerInfo(Customer $user): string
    {
        return \sprintf(
            "user-type: %s \nid: %d \nname: %s \nbalance: %.2f \npurchase-count: %d\n\n",
            (new \ReflectionClass(Customer::class))->getShortName(),
            $user->getId(),
            $user->getName(),
            $user->getBalance(),
            $user->getPurchaseCount()
        );
    }

    /**
     * @param Seller $user
     *
     * @return string
     */
    private function getSellerInfo(Seller $user): string
    {
        return \sprintf(
            "user-type: %s \nid: %d \nname: %s \nearnings-balance: %.2f \nproduct-count: %d\n\n",
            (new \ReflectionClass(Seller::class))->getShortName(),
            $user->getId(),
            $user->getName(),
            $user->getEarningsBalance(),
            $user->getProductCount()
        );
    }

    /**
     * @param Administrator $user
     *
     * @return string
     */
    private function getAdministratorInfo(Administrator $user): string
    {
        return \sprintf(
            "user-type: %s \nid: %d \nname: %s \npermissions: %s\n\n",
            (new \ReflectionClass(Administrator::class))->getShortName(),
            $user->getId(),
            $user->getName(),
            \implode(', ', $user->getPermissions())
        );
    }
}