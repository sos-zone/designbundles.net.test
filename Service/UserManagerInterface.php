<?php

namespace App\Service;

use App\Entity\BaseUser;

interface UserManagerInterface
{
    public function getUserInfo(BaseUser $user): string;
}