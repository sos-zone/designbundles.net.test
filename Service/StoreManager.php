<?php

namespace App\Service;

class StoreManager
{
    /*
    * @var DatabaseManager $dbManager
    */
    protected $dbManager = null;

    /*
    * @param DatabaseManager $dbManager
    */
    public function __construct(DatabaseManager $dbManager)
    {
        $this->dbManager = $dbManager;
    }

    /*
    * @param int $storeId
    *
    * return float
    */
    public function getStoreEarnings(int $storeId): float
    {
        $totalAmount = 0;

        $storeOrderItems = $this->getOrderItemsByStoreId($storeId);

        foreach ($storeOrderItems as $orderItem) {
            $totalAmount += $orderItem['price'];
        }

        return $totalAmount;
    }

    /*
    * @param int $storeId
    *
    * return array
    */
    protected function getOrderItemsByStoreId(int $storeId): array
    {
        $query = 'SELECT p.price as price FROM OrderItem as oi
                    LEFT JOIN Product as p ON oi.product_id = p.id
                    LEFT JOIN Store as s ON p.store_id = s.id
                    WHERE s.id = :store';

        return $this->dbManager->getData($query, [
            'store' => $storeId,
        ]);
    }
}



