<?php

namespace App\Service;

use App\Entity\Administrator;
use App\Entity\Customer;
use App\Entity\Seller;

class UserFactory
{
    /**
     * @param int $id
     * @param string $name
     * @param float $balance
     * @param int $purchaseCount
     *
     * @return Customer
     */
    public function createCustomer(
        int $id,
        string $name,
        float $balance,
        int $purchaseCount
    ): Customer {
        return new Customer($id, $name, $balance, $purchaseCount);
    }

    /**
     * @param int $id
     * @param string $name
     * @param float $earningsBalance
     * @param int $productCount
     *
     * @return Seller
     */
    public function createSeller(
        int $id,
        string $name,
        float $earningsBalance,
        int $productCount
    ): Seller {
        return new Seller($id, $name, $earningsBalance, $productCount);
    }

    /**
     * @param int $id
     * @param string $name
     * @param array $permissions
     *
     * @return Administrator
     */
    public function createAdministrator(
        int $id,
        string $name,
        array $permissions
    ): Administrator {
        return new Administrator($id, $name, $permissions);
    }
}