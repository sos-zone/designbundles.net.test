<?php

require_once('Entity/BaseUser.php');
require_once('Entity/Customer.php');
require_once('Entity/Seller.php');
require_once('Entity/Administrator.php');
require_once('Service/UserManagerInterface.php');
require_once('Service/UserManager.php');
require_once('Service/UserFactory.php');

use App\Service\UserManager;
use App\Service\UserFactory;

$userManager = new UserManager();
$userFactory = new UserFactory();

// create users:
$customer = $userFactory->createCustomer(1, 'John', 100.55, 34);
$seller = $userFactory->createSeller(2, 'Alex', 550.87, 781);
$administrator = $userFactory->createAdministrator(3, 'Arnold', ['reports', 'sales', 'users']);

// output user's info:
echo $userManager->getUserInfo($customer);
echo $userManager->getUserInfo($seller);
echo $userManager->getUserInfo($administrator);
