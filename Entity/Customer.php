<?php

namespace App\Entity;

class Customer extends BaseUser
{
    /**
     * @var float
     */
    private $balance = 0;

    /**
     * @var int
     */
    private $purchaseCount = 0;

    /**
     * @param int $id
     * @param string $name
     * @param float $balance
     * @param int $purchaseCount
     */
    public function __construct(
        int $id,
        string $name,
        float $balance,
        int $purchaseCount
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->balance = $balance;
        $this->purchaseCount = $purchaseCount;
    }

    /**
     * @return float
     */
    public function getBalance(): float
    {
        return $this->balance;
    }

    /**
     * @return int|null
     */
    public function getPurchaseCount(): ?int
    {
        return $this->purchaseCount;
    }
}