<?php

namespace App\Entity;

class Seller extends BaseUser
{
    /**
     * @var float
     */
    private $earningsBalance = 0;

    /**
     * @var int
     */
    private $productCount = 0;

    /**
     * @param int $id
     * @param string $name
     * @param float $earningsBalance
     * @param int $productCount
     */
    public function __construct(
        int $id,
        string $name,
        float $earningsBalance,
        int $productCount
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->earningsBalance = $earningsBalance;
        $this->productCount = $productCount;
    }

    /**
     * @return float|null
     */
    public function getEarningsBalance(): ?float
    {
        return $this->earningsBalance;
    }

    /**
     * @return int|null
     */
    public function getProductCount(): ?int
    {
        return $this->productCount;
    }
}