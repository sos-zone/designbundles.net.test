<?php

namespace App\Entity;

class Administrator extends BaseUser
{
    private $permissions = [];

    public function __construct(
        int $id,
        string $name,
        array $permissions
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->permissions = $permissions;
    }

    public function getPermissions(): array
    {
        return array_unique($this->permissions);
    }
}