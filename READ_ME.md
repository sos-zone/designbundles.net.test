## I). SQL tests:
##### 1. Show Stores, that have products with Christmas, Winter Tags:
```
SELECT s.name FROM Store AS s
    LEFT JOIN Product AS p ON p.store_id = s.id
    LEFT JOIN TagConnect AS tc ON tc.product_id = p.id
    LEFT JOIN Tag AS t ON t.id = tc.tag_id
    WHERE t.tag_name IN ('Christmas', 'Winter')
    GROUP BY s.id;
```
    
##### 2. Show Users, that never bought Product from Store with id == 5
```
SELECT u.name FROM User AS u
    LEFT JOIN `Order` AS o ON o.customer_id = u.id
    LEFT JOIN OrderItem oi ON oi.order_id = o.id
    LEFT JOIN Product p ON p.id = oi.product_id
    WHERE p.store_id != 5
    GROUP BY u.id;
```
    
##### 3. Show Users, that had spent more than $1000
```
SELECT u.name, SUM(p.price) as spent_money
    FROM User AS u
    LEFT JOIN `Order` AS o ON o.customer_id = u.id
    LEFT JOIN OrderItem oi ON oi.order_id = o.id
    LEFT JOIN Product p ON p.id = oi.product_id
    GROUP BY u.id
    HAVING spent_money > 1000;
```

##### 4. Show Stores, that have not any Sells
```
SELECT s.name
    FROM Store AS s
    LEFT JOIN Product AS p ON p.store_id = s.id
    LEFT JOIN OrderItem oi ON p.id = oi.product_id
    WHERE oi.id IS NULL
    GROUP BY s.name;
```
    
##### 5. Show Mostly sold Tags
```
SELECT t.tag_name, COUNT(*) AS selled_products from Tag AS t
    LEFT JOIN TagConnect tc ON t.id = tc.tag_id
    LEFT JOIN OrderItem oi ON tc.product_id = oi.product_id
    GROUP BY t.id
    ORDER BY selled_products DESC LIMIT 2;
```
##### 6. Show Monthly Store Earnings Statistics
##### TODO: need to check!
```
SELECT s.name, YEAR(o.order_date), MONTH(o.order_date), SUM(p.price)
    FROM Store AS s
    LEFT JOIN Product AS p ON p.store_id = s.id
    LEFT JOIN OrderItem oi on p.id = oi.product_id
    LEFT JOIN `Order` o on o.id = oi.order_id
    WHERE oi.id IS NOT NULL
    GROUP BY s.name, YEAR(o.order_date), MONTH(o.order_date)
    HAVING sum(p.price) > 0
    ORDER BY s.name, YEAR(o.order_date), MONTH(o.order_date);
```
    
    
## III) (look into App\Service\StoreManager class)
Syntax errors:
- use '' instead of ‘‘;
- after self use double colon instead of one (self::getTotalUniqueTags());
- Using $this when not in object context (public static function getTotalUniqueTags());
- logic with 'Christmas' and 'Free' ProductTagCost coefficients is not present. Is it requared?
- use separate methods for $storeManager->getTotalStoreAmount() and $storeManager->getStoreAmountByProductTags();
- method calculateStoreEarnings() should return float;
- use return type declaration;
- $totalAmount will stay at the top of method, otherwise this value will reload at each iteration;
- StoreManager->getOrderItems will return all store's products (need only products for $storeId)

Not optimal code:
- calculateStoreEarnings should return float value, will be better rename it to the something like getStoreEarnings();
- use more logic variable names: use $orderItem instead of $item in foreach cycle;
- by the using OOP the best choice will be use objects instead of associative arrays;

Optimization:
- use less count of queries into db;
- select only needed fields;
- handle exception if store by $storeId is not found;
- trying to use not nested foreach loops;